FROM quay.io/fedora/fedora:35

RUN dnf -y update && \
    dnf -y install \
        alsa-lib-devel \
        atk-devel \
        expat-devel \
        bison \
        ccache \
        clang \
        createrepo_c \
        cups-devel \
        dbus-devel \
        elfutils \
        fakeroot \
        meson \
        mesa-libgbm-devel \
        ninja-build \
        findutils \
        git \
        glib2-devel \
        gperf \
        gtk3-devel \
        java-1.8.0-openjdk \
        libXScrnSaver-devel \
        libva-devel \
        libstdc++-static \
        make \
        nodejs \
        python2.7 \
        libgnome-keyring-devel \
        nss-devel \
        pango-devel \
        patch \
        perl-FindBin \
        pciutils-devel \
        pulseaudio-libs-devel \
        quilt \
        rpm-build \
        wget \
        libarchive \
        libdrm-devel \
        libXtst-devel \
        xcb-proto \
        xz \
    && dnf clean all

COPY uuid-1.6.2-52.fc35.ppc64le.rpm uuid-c++-1.6.2-52.fc35.ppc64le.rpm uuid-c++-devel-1.6.2-52.fc35.ppc64le.rpm uuid-dce-1.6.2-52.fc35.ppc64le.rpm uuid-dce-devel-1.6.2-52.fc35.ppc64le.rpm uuid-devel-1.6.2-52.fc35.ppc64le.rpm uuid-perl-1.6.2-52.fc35.ppc64le.rpm /tmp

RUN dnf -y install /tmp/*.rpm && ln -sf /usr/lib64/libossp-uuid.a /usr/lib64/libuuid.a

# the xcb-proto installes xcbgen into python3 packages only
# so we need to copy it to python2 to work around the missing xcbgen issue
RUN cp -r /usr/lib/python3.10/site-packages/xcbgen /usr/lib/python2.7/site-packages && \
      rm -f /usr/bin/python && \
      ln -s /usr/bin/python2.7 /usr/bin/python &&\
      python2.7 -m ensurepip --upgrade

RUN mkdir -p /workdir
WORKDIR /workdir

CMD ["/usr/bin/make", "-w", "-j16"]
